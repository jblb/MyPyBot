#! /usr/bin/env python
# -*- coding: utf-8 -*-

# BotIRC by jerome@jblb.net
#
#

from twisted.words.protocols import irc
from twisted.internet import reactor, protocol



# class IRCBot

class IRCBot(irc.IRCClient):
    """An IRC bot."""

    def __init__ (self, nickname):
        self.nickname = nickname

    def connectionMade(self):
        irc.IRCClient.connectionMade(self)

    def connectionLost(self, reason):
        irc.IRCClient.connectionLost(self, reason)

    # callbacks for events

    def signedOn(self):
        """Called when bot has succesfully signed on to server."""
        self.join(self.factory.channel)
        print("connecté a %s" % self.factory.channel)

    def joined(self, channel):
        """This will get called when the bot joins the channel."""
        msg = "Nous sommes legion, nous sommes %s !" % self.nickname
        self.msg(channel,msg)


    def privmsg(self, user, channel, msg):
        """This will get called when the bot receives a message."""
        user = user.split('!', 1)[0]
        # print("<%s> %s" % (user, msg))
        if user != self.nickname:
            rep_msg = user + " a parlé !"
            # self.msg(channel,rep_msg)
        if user == "bcazeneuve" :
            rep_msg = "ta gueule " + user
            self.msg(channel,rep_msg)

        # Check to see if they're sending me a private message
        if channel == self.nickname:
            msg = "Je ne parlerai qu'en présence de mon avocat !"
            self.msg(user, msg)
            return

        # Otherwise check to see if it is a message directed at me
        if msg.startswith(self.nickname + ":"):
            # msg = "%s: j'ai le droit au silence et j'en use " % user
            msg = "chut %s: je dors "  % user
            self.msg(user, msg)


    def action(self, user, channel, msg):
        """This will get called when the bot sees someone do an action."""
        user = user.split('!', 1)[0]
        print("* %s %s" % (user, msg))

    # irc callbacks
    def kickedFrom(self, channel, kicker, message):
        """ Called when I am kicked from a channel. """
        print("%s kicked me from %s whih message %s" % (kicker, channel, message))
        self.join(self.factory.channel)

    def userJoined(self, user, channel):
        """ Called when I see another user joining a channel."""
        msg = "Bonjour %s, comment vas tu ?" % user
        #self.msg(channel, msg)
        print" %s " % user


    def irc_NICK(self, prefix, params):
        """Called when an IRC user changes their nickname."""
        old_nick = prefix.split('!')[0]
        new_nick = params[0]
        # msg = "Alors " + new_nick+ ", on essai de se cacher !"
        # self.msg(self.factory.channel, msg)
        print("%s is now known as %s" % (old_nick, new_nick))


    # For fun, override the method that determines how a nickname is changed on
    # collisions. The default method appends an underscore.
    def alterCollidedNick(self, nickname):
        """
        Generate an altered version of a nickname that caused a collision in an
        effort to create an unused related name for subsequent registration.
        """
        return nickname + '^'


class JblbBotFactory(protocol.ClientFactory):
    """A factory for Bots.

    A new protocol instance will be created each time we connect to the server.
    """

    def __init__(self, channel, nickname):
        self.channel = channel
        self.nickname = nickname


    def buildProtocol(self, addr):
        p = IRCBot(self.nickname)
        p.factory = self
        return p

    def clientConnectionLost(self, connector, reason):
        """If we get disconnected, reconnect to server."""
        print "Disconnected: ", reason 
        connector.connect()

    def clientConnectionFailed(self, connector, reason):
        print "connection failed:", reason
        reactor.stop()


if __name__ == '__main__':
    # initialize logging

    # create factory protocol and application
    f = JblbBotFactory("#haum", "BotDebout")

    # connect factory to this host and port
    reactor.connectTCP("irc.freenode.net", 6667, f)

    # run bot
    reactor.run()
